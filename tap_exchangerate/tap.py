"""exchangerate tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers

# TODO: Import your custom stream types here:
from tap_exchangerate.streams import (
    # LatetRateStream,
    HistoricalRateStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    # LatetRateStream,
    HistoricalRateStream
]


class Tapexchangerate(Tap):
    """exchangerate tap class."""
    name = "tap-exchangerate"

    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "base",
            th.StringType,
            required=False,
            description="[optional] Changing base currency. Enter the three-letter currency code of your preferred base currency.example:base=USD"
        ),
        th.Property(
            "symbols",
            th.StringType,
            required=False,
            description="[optional] Enter a list of comma-separated currency codes to limit output currencies.example:symbols=USD,EUR,CZK"
        ),
        th.Property(
            "amount",
            th.StringType,
            required=False,
            description="[optional] The amount to be converted.example:amount=1200"
        ),
        th.Property(
            "callback",
            th.StringType,
            required=False,
            description="[optional] API comes with support for JSONP Callbacks. This feature enables you to specify a function name, pass it into the API's callback GET parameter and cause the API to return your requested API response wrapped inside that function.example:callback=functionName"
        ),
        th.Property(
            "places",
            th.StringType,
            required=False,
            description="[optional] Round numbers to decimal place.example:places=2"
        ),
        th.Property(
            "format",
            th.StringType,
            required=False,
            description="[optional] If respone success, then you can format output to XML, CSV or TSV.example:format=tsv"
        ),
        th.Property(
            "source",
            th.StringType,
            required=False,
            description="[optional] You can switch source data between (default) forex, bank view or crypto currencies.- example of European Central Bank source (list of bank sources ↗): source=ecb - example of Crypto currencies source (list of crypto currencies ↗): source=crypto"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]
