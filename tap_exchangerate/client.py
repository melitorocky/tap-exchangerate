"""Custom client handling, including exchangerateStream base class."""

import requests
from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk.streams import Stream


class exchangerateStream(Stream):
    """Stream class for exchangerate streams."""

    def get_records(self, context: Optional[dict]) -> Iterable[dict]:
        """Return a generator of row-type dictionary objects.

        The optional `context` argument is used to identify a specific slice of the
        stream if partitioning is required for the stream. Most implementations do not
        require partitioning and should ignore the `context` argument.
        """
        streamName = type(self).__name__

        # map the inputs to the function blocks
        if streamName == 'LatetRateStream':
            return self.LatetRateStream(context)
        elif streamName == 'HistoricalRateStream':
            return self.HistoricalRateStream(context)
        else:
            print(streamName + ' not found..')
            quit()

    def HistoricalRateStream(self,context):

        # currencies = ["AED","AFN","ALL","AMD","ANG","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTC","BTN","BWP","BYN","BZD","CAD","CDF","CHF","CLF","CLP","CNH","CNY","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GGP","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","IMP","INR","IQD","IRR","ISK","JEP","JMD","JOD","JPY","KES","KGS","KHR","KMF","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MRU","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","SSP","STD","STN","SVC","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VES","VND","VUV","WST","XAF","XAG","XAU","XCD","XDR","XOF","XPD","XPF","XPT","YER","ZAR","ZMW","ZWL"]
        currencies = ["AED","AUD","BTC","CAD","CHF","CNY","DKK","EUR","GBP","HKD","JPY","NOK","PLN","RUB","SEK","TRY","TWD","USD"]
        url = self.config["url"]
        symbols = self.config["symbols"]
        date = self.config["date"]
        access_key = self.config["access_key"]

        url = url+"historical"
        result = []
        for currency in currencies:
            params = {
                "source": currency,
                "symbols": symbols,
                "date": date,
                "access_key": access_key
                }
            response = requests.get(url, params=params)
            data = response.json()

            if data['success'] == True:
                rates = data['quotes']
                for rate in list(rates):
                    rates[rate[3:]] = rates.pop(rate)
                rates['currency_code'] = currency
                rates['date'] = date + ' 00:00:00'
                result.append(rates)
            else:
                print("Non funziona Historical")

        return result


        # def LatetRateStream(self,context):

    #     currencies = ["AED","AFN","ALL","AMD","ANG","AOA","ARS","AUD","AWG","AZN","BAM","BBD","BDT","BGN","BHD","BIF","BMD","BND","BOB","BRL","BSD","BTC","BTN","BWP","BYN","BZD","CAD","CDF","CHF","CLF","CLP","CNH","CNY","COP","CRC","CUC","CUP","CVE","CZK","DJF","DKK","DOP","DZD","EGP","ERN","ETB","EUR","FJD","FKP","GBP","GEL","GGP","GHS","GIP","GMD","GNF","GTQ","GYD","HKD","HNL","HRK","HTG","HUF","IDR","ILS","IMP","INR","IQD","IRR","ISK","JEP","JMD","JOD","JPY","KES","KGS","KHR","KMF","KPW","KRW","KWD","KYD","KZT","LAK","LBP","LKR","LRD","LSL","LYD","MAD","MDL","MGA","MKD","MMK","MNT","MOP","MRO","MRU","MUR","MVR","MWK","MXN","MYR","MZN","NAD","NGN","NIO","NOK","NPR","NZD","OMR","PAB","PEN","PGK","PHP","PKR","PLN","PYG","QAR","RON","RSD","RUB","RWF","SAR","SBD","SCR","SDG","SEK","SGD","SHP","SLL","SOS","SRD","SSP","STD","STN","SVC","SYP","SZL","THB","TJS","TMT","TND","TOP","TRY","TTD","TWD","TZS","UAH","UGX","USD","UYU","UZS","VES","VND","VUV","WST","XAF","XAG","XAU","XCD","XDR","XOF","XPD","XPF","XPT","YER","ZAR","ZMW","ZWL"]
    #     url = self.config["latest"]["url"]
    #     symbols = self.config["latest"]["symbols"]

    #     result = []
    #     for currency in currencies:
    #         params = {
    #             "base": currency,
    #             "symbols": symbols
    #             }
    #         response = requests.get(url, params=params)
    #         data = response.json()

    #         if data['success'] == True:
    #             rates = data['rates']
    #             rates['currency_code'] = currency
    #             result.append(rates)
    #         else:
    #             print("Non funziona Latest")

    #     return result